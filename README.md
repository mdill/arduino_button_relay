# Arduino Relay Test

## Purpose

This simple sketch is designed to turn a relay on and off whenever the user
presses a pushbutton.

It was originally created for my friend, Sean, who needed help with this project.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/arduino_button_relay.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/arduino_button_relay/src/72fcee241b49dfbd9bcf5ab6c7e6ee98dd2917d9/LICENSE.txt?at=master) file for
details.

