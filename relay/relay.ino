/*
20151114 - Michael Dill

An Arduino sketch designed to take a pushbutton input and control a powered
relay.

Initially the relay is set to HIGH (on) and is turned off once the button is
pressed.

Once the button is released, the relay is turned back on again.
*/

// Set pin numbers
byte buttonPin = 2;  // The pushbutton pin
byte ledPin =  13;  // The LED pin

// Variables which will change
byte buttonState = 0;  // Variable for reading the pushbutton status
byte OldButtonState = 0;  // Variable to compare read state to

void setup() {
  // Initialize the LED pin as an output
  pinMode(ledPin, OUTPUT);

  // Set LED pin as on
  digitalWrite(ledPin, HIGH);
  
  // Initialize the pushbutton pin as an input
  pinMode(buttonPin, INPUT);

  pinMode(12, OUTPUT);
  digitalWrite(12, HIGH);
}

void loop(){
  // Read the state of the pushbutton value
  buttonState = digitalRead(buttonPin);

  // Check if the pushbutton is pressed
  if (buttonState != OldButtonState) {
    OldButtonState = buttonState;
    ChangeRelay(OldButtonState);
  }
}

void ChangeRelay(byte weh){
  // If it is, the buttonState is HIGH
  if (weh == 1) {
    digitalWrite(ledPin, LOW);
  }
  else {
    // Turn LED off
    digitalWrite(ledPin, HIGH);
  }
  return;
}
